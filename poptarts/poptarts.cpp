// CS2S560 Data Structures and Algorithms - Course Work
// Student: 15007561
// 24 March 2017
#include <iostream>
#include <string>
#include <vector>

using namespace std;

class StateContext;

//////////////////////////////////////////////////////////////////////////
// Appendix A - Do not change code!

// Represent the states of the dispenser that will be used to transition between the state objects.
enum state { Out_Of_Poptart, No_Credit, Has_Credit, Dispenses_Poptart };

// Represent the parameters that will contain information to be shared between states contained in the state context.
enum stateParameter { No_Of_Poptarts, Credit, Cost_Of_Poptart };

// The base interface for states which will be inherited by the poptart state.
// Contains a pointer to the current context. This will allow the state to transition
// to the current context to the next state.
class State
{
protected:
	StateContext* CurrentContext;

public:
	State(StateContext* Context)
	{
		CurrentContext = Context;
	}
	virtual ~State(void) {}
	virtual void transition(void) {}
};

// This interface contains the available states and the state parameters.
// This will be used to keep track of the current state, change the state
// and will manage the state parameters used by the states.
// This will be inherited by the dispenser 
class StateContext
{
protected:
	State* CurrentState = nullptr;
	int stateIndex = 0;
	vector<State*> availableStates;
	vector<int> stateParameters;

public:
	virtual ~StateContext(void);
	virtual void setState(state newState);
	virtual int getStateIndex(void);
	virtual void setStateParam(stateParameter SP, int value);
	virtual int getStateParam(stateParameter SP);
};

// Destructor that will delete the state objects and clean up the states and parameters vectors.
StateContext::~StateContext(void)
{
	for (int i = 0; i < this->availableStates.size(); i++) delete this->availableStates[i];
	this->availableStates.clear();
	this->stateParameters.clear();
}

// Description: Sets the current state in the state context. This will be done 
// by the current state when an appropriate trasition happens. Such as
// the No Credit state receiving credit will then set the state to Has Credit.
// Parameters: The state to transition to using the state enums.
// Returns: nothing
void StateContext::setState(state newState)
{
	this->CurrentState = availableStates[newState];
	this->stateIndex = newState;
	this->CurrentState->transition();
}

// Description: Retrieves the index that represents the current state.
// This could be used by the dispenser to check which state it
// currently is in since it inherits this method.
// Parameters: none
// Returns: The state index as an integer.
int StateContext::getStateIndex(void)
{
	return this->stateIndex;
}

// Description: Sets the state parameter given with the value past. This is will
// be used by the states to share information between each state. Such as the
// amount of credit the dispenser currently holds.
// Parameters: Which state parameter to set, using a stateParameter enum value,
// then the value of the parameter.
// Returns: nothing
void StateContext::setStateParam(stateParameter SP, int value)
{
	this->stateParameters[SP] = value;
}

// Description: Retrieves the value of the given state. The states will
// be able to retrieve shared information this way. Such as checking
// how many poptarts the dispenser has.
// Parameters: The state parameter to retrieve using a stateParameter enum value.
// Returns: The value of the parameter past as an integer.
int StateContext::getStateParam(stateParameter SP)
{
	return this->stateParameters[SP];
}

// Interface for the transactions that each state can possibly use, 
// depending on what is permitted by the current state.
// The dispenser and the states will use this class. These are the methods
// that the dispenser will run based on the current state.
// Each state will overwrite these methods according its design.
class Transition
{
public:
	virtual bool insertMoney(int) { cout << "Error!" << endl; return false; }
	virtual bool makeSelection(int) { cout << "Error!" << endl; return false; }
	virtual bool moneyRejected(void) { cout << "Error!" << endl; return false; }
	virtual bool addPoptart(int) { cout << "Error!" << endl; return false; }
	virtual bool dispense(void) { cout << "Error!" << endl; return false; }
};

// Interface for the poptart states which will allow the poptart state to
// access the state context to set the state, set and view state properties,
// and execute the appropriate transition method for that state.
class PoptartState : public State, public Transition
{ 
public:
	PoptartState(StateContext* Context) : State(Context) {}
};

// The OutOfPoptart state will be the current state when the number of 
// poptarts in the state parameter, No_Of_Poptarts, is zero.
// The transition method will be the addPoptart() method. This method will
// set the No_Of_Poptarts state parameter to the given number.
// Then the dispenser's state will be set to the NoCredit state.
class OutOfPoptart : public PoptartState
{
public:
	OutOfPoptart(StateContext* Context) : PoptartState(Context) {}
	bool insertMoney(int money);
	bool makeSelection(int option);
	bool moneyRejected(void);
	bool addPoptart(int number);
	bool dispense(void);
};

// The NoCredit state will be the current state when the No_Of_Poptarts 
// state parameter is greater than zero. The transition method for this state
// will be insertMoney(). The insertMoney() method will set the Credit state parameter
// to the credit given. Then the dispenser's state will be set to HasCredit.
class NoCredit : public PoptartState
{
public:
	NoCredit(StateContext* Context) : PoptartState(Context) {}
	bool insertMoney(int money);
	bool makeSelection(int option);
	bool moneyRejected(void);
	bool addPoptart(int number);
	bool dispense(void);
};

// The HasCredit state will be the current state when the Credit state parameter
// is greater than zero. The transition methods will be moneyRejected() and makeSelection().
// When moneyRejected() is executed, the Credit state parameter will go to zero.
// And the state will change to NoCredit.
// If makeSelection() method is executed with a correct selection and there is enough
// credit for the selected item, then the product in the dispenser will be set to
// the selection made. Then the dispenser's state will transition to the DispensesPoptart state.
class HasCredit : public PoptartState
{
public:
	HasCredit(StateContext* Context) : PoptartState(Context) {}
	bool insertMoney(int money);
	bool makeSelection(int option);
	bool moneyRejected(void);
	bool addPoptart(int number);
	bool dispense(void);
};

// The DispensesPoptart state will be the current state after a product has been selected.
// The transition method will be dispense(), which will set the dispenser's itemDispensed
// to true and itemRetrieved to false and reduce No_Of_Poptarts by one. Then dispense() will check
// if No_Of_Poptarts state parameter is greater than zero. If it is greater than zero,
// then it will check if the Credit parameter is greater than zero. If Credit is greater than zero, then 
// it will trasition to the HasCredit state to allow the user to eject their money, add money or make a selection.
// If credit is zero, then we transition back to the NoCredit state and wait for credit to be inserted.
// If No_Of_Poptarts parameter is zero, then we transition to the OutOfPoptart state and wait for
// poptarts to be added.
class DispensesPoptart : public PoptartState
{
public:
	DispensesPoptart(StateContext* Context) : PoptartState(Context) {}
	bool insertMoney(int money);
	bool makeSelection(int option);
	bool moneyRejected(void);
	bool addPoptart(int number);
	bool dispense(void);
};

// The product interface for the poptart to inherit.
// This will store the product description and cost.
// Using polymorphism to allow a list of products
// contain any subtype of product.
class Product
{
protected:
	string product_description;
	int itemCost = 0;
public:
	virtual int cost(void);
	virtual string description(void);
};

// The poptart interface that inherits the product interface.
// Each base poptart will inherit this interface. This will
// allow each base poptart to store their description and cost
// and be seen as a product and a poptart.
class Poptart : public Product
{
public:
	Poptart(void);
};

// The poptart dispenser that will be the state context, keeping track
// of the current state and executing the transactions of the current state.
// It is also responsible for storing the selected product and 
// keeping track of whether the product has been dispensed or retrieved.
class Poptart_Dispenser : public StateContext, public Transition
{
	friend class DispensesPoptart;
	friend class HasCredit;
private:
	PoptartState* PoptartCurrentState = nullptr;
	bool itemDispensed = false; // indicates whether a product is there to be retrieved
	Product* DispensedItem = nullptr;
	bool itemRetrieved = false; // indicates whether a product has been retrieved
public:
	Poptart_Dispenser(int inventory_count);
	~Poptart_Dispenser(void);
	bool insertMoney(int money);
	bool makeSelection(int option);
	bool moneyRejected(void);
	bool addPoptart(int number);
	bool dispense(void);
	Product* getProduct(void);
	virtual void setStateParam(stateParameter SP, int value);
	virtual int getStateParam(stateParameter SP);
};

// Delete the dispensed product on destruction if it was not retrieved.
Poptart_Dispenser::~Poptart_Dispenser(void)
{
	if (!this->itemRetrieved)
	{
		delete this->DispensedItem;
	}
}

// Description: Will call the insertMoney() method on the current state object.
// If the current poptart state is NoCredit or HasCredit, then the amount will be set in the Credit state parameter.
// Parameter: The amount of money to insert.
// Returns: If money was inserted then it will return true, otherwise false.
bool Poptart_Dispenser::insertMoney(int money)
{
	PoptartCurrentState = (PoptartState*) this->CurrentState;
	return this->PoptartCurrentState->insertMoney(money);
}

// Description: Will call the makeSelection() method on the current state object.
// HasCredit is the only state that will allows a selection to be made.
// Parameters: The value that represents the selection made.
// Returns: HasCredit is the only state that will return. Will return true if
// the selection is valid and there is enough credit for the selection, otherwise returns false.
bool Poptart_Dispenser::makeSelection(int option)
{
	PoptartCurrentState = (PoptartState*) this->CurrentState;
	return this->PoptartCurrentState->makeSelection(option);
}

// Description: Will call the moneyRejected() method on the current state object.
// This will notify the user that their money was rejected, setting the Credit parameter to zero.
// Parameters: None
// Returns: Returns true if money was rejected, otherwise false.
bool Poptart_Dispenser::moneyRejected(void)
{
	PoptartCurrentState = (PoptartState*) this->CurrentState;
	return this->PoptartCurrentState->moneyRejected();
}

// Description: Will call the addPoptart() method on the current state object.
// The OutOfPoptart will be the only state that can add poptarts. This will
// set the No_Of_Poptarts state parameter to the value specified.
// Parameter: The number of poptarts to add.
// Returns: Will return true if the poptarts were added, otherwise returns false.
bool Poptart_Dispenser::addPoptart(int number)
{
	PoptartCurrentState = (PoptartState*) this->CurrentState;
	return this->PoptartCurrentState->addPoptart(number);
}

// Description: Will call the dispense() method on the current state object.
// The only state that will dispense an item is the DispensesPoptart state.
// This assumes that a selection was already made and the DispensedItem
// has been set by the selection.
// Parameters: None
// Returns: If a poptart is dispensed, then it will return true, otherwise false.
bool Poptart_Dispenser::dispense(void)
{
	PoptartCurrentState = (PoptartState*) this->CurrentState;
	return this->PoptartCurrentState->dispense();
}

// Description: If there is an DisepnsedItem, this method will set itemDispensed to false,
// set itemRetrieved to true, and return the DispensedItem selected.
// Parameters: None
// Returns: Will return a poptart product that was dispensed.
Product* Poptart_Dispenser::getProduct(void)
{
	if (this->itemDispensed)
	{
		this->itemDispensed = false;
		this->itemRetrieved = true;
		return this->DispensedItem;
	}

	return nullptr;
}

// Description: Will set the state parameter specified my the state parameter.
// This will allow states to set state parameters.
// This will not allow Cost_Of_Poptart to be set, since it will retrieve the cost
// from the selected poptart.
// Parameters: The state parameter using an stateParameter enum and the value of the parameter.
// Returns: nothing
void Poptart_Dispenser::setStateParam(stateParameter SP, int value)
{
	if (SP == Cost_Of_Poptart) return;
	this->stateParameters[SP] = value;
}

// Description: Will retrieve the given state's value.
// This will allow states to get state parameter values.
// If the getting the Cost_Of_Poptart, it will retrieve it from the DispensedItem.
// Otherwise it will retrieve the value from the stateParameters vector.
// Parameters: The state parameter to set using an stateParameter enum.
// Returns: The value of the state parameter, such as the value of Credit.
int Poptart_Dispenser::getStateParam(stateParameter SP)
{
	if (SP == Cost_Of_Poptart)
	{
		if (DispensedItem == nullptr) return 0;
		return DispensedItem->cost();
	}
	return this->stateParameters[SP];
}
// End of Appendix A
//////////////////////////////////////////////////////////////////////////


// Description: Constructor used to initialise the Poptart_Dispenser by
//				adding the available states, setting state properties,
//				setting the state to "out of poptarts",	then adding poptarts 
//				to the inventory if the inventory count is greater than zero.
// Parameter: Number of poptarts to add to the inventory.
Poptart_Dispenser::Poptart_Dispenser(int inventory_count)
{
	// These are the states the dispenser will tranistion between.
	this->availableStates.push_back(new OutOfPoptart(this));
	this->availableStates.push_back(new NoCredit(this));
	this->availableStates.push_back(new HasCredit(this));
	this->availableStates.push_back(new DispensesPoptart(this));

	// These are the state parameters used to set and view how much
	// inventory is available and how much credit has been entered
	this->stateParameters.push_back(0); // number of poptarts
	this->stateParameters.push_back(0); // credit total

	// We start in a state of being out of poptarts.
	this->setState(Out_Of_Poptart);

	// If the inventory count is greater than zero, then add
	// that number of poptarts to the inventory. This will change the
	// state to no_credit.
	if (inventory_count > 0)
	{
		this->addPoptart(inventory_count);
	}
}


// Description: Retrieves the cost of the product.
// Parameters: None
// Returns: Will return the cost of the product as an integer.
int Product::cost(void)
{
	return itemCost;
}

// Description: Retrieves the product description.
// Parameters: None
// Returns: Will return the product's description as a string.
string Product::description(void)
{
	return product_description;
}

// Description: Constructor that will set the 
//				product description and item cost.
// Parameters: None
// Returns: Nothing
Poptart::Poptart(void)
{
	product_description = "Poptart";
	itemCost = 100;
}

// The base types of poptarts are the poptarts that a user can select.
// Each of these classes sets its own description and cost.
// The cost will be used to when determining if the dispenser Credit 
// is sufficient for the selected poptart.
// The description will be used to communicate which poptart has been selected.

// Plain poptart base
class Plain : public Poptart
{
public:
	Plain()
	{
		product_description = "Plain Poptart";
		itemCost = 100;
	}
};

// Spicy poptart base
class Spicy : public Poptart
{
public:
	Spicy()
	{
		product_description = "Spicy Poptart";
		itemCost = 150;
	}
};

// Chocolate poptart base
class Chocolate : public Poptart
{
public:
	Chocolate()
	{
		product_description = "Chocolate Poptart";
		itemCost = 200;
	}
};

// Coconut poptart base
class Coconut : public Poptart
{
public:
	Coconut()
	{
		product_description = "Coconut Poptart";
		itemCost = 200;
	}
};

// Fruity poptart base
class Fruity : public Poptart
{
public:
	Fruity()
	{
		product_description = "Fruity Poptart";
		itemCost = 200;
	}
};


//////////////////////////////////////////////////////////////////////////
// Out of poptart state

// Description: Will reject money inserted because there are no poptarts to purchase.
// Parameter: Money entered by the user.
// Returns: Will return false because the money was rejected.
bool OutOfPoptart::insertMoney(int money)
{
	cout << "Error! Out of poptarts! Not accepting money!" << endl;
	this->moneyRejected();
	return false;
}

// Description: Notifies the user that a selection can't be made because there are no poptarts.
// Parameter: Option selected by the user.
// Returns: Will return false because there are no poptarts to select.
bool OutOfPoptart::makeSelection(int option)
{
	cout << "Error! Out of poptarts! Selection can't be made." << endl;
	return false;
}

// Description: Ejects money inserted.
// Parameters: none
// Returns: Will return true since the money was rejected.
bool OutOfPoptart::moneyRejected(void)
{
	cout << "Ejecting money!" << endl;
	return true;
}

// Description: Adds the number of poptarts given to the inventory by
// setting the No_Of_Poptarts state parameter.
// Then transition the state of the dispenser to NoCredit,
// since there are now poptarts available.
// Parameter: The number of poptarts to add.
// Returns: Return true if poptarts were added. 
//			Return false if the number of poptarts is less than one.
bool OutOfPoptart::addPoptart(int number)
{
	// Check if the number of poptarts added is valid
	if (number > 0)
	{
		// Update the inventory with the number of poptarts added.
		// Now that we have poptarts, transition to the No_Credit state
		// so that a user can insert credit to purchase a poptart.
		this->CurrentContext->setStateParam(No_Of_Poptarts, number);
		cout << "Added " << number << " poptarts" << endl;
		this->CurrentContext->setState(No_Credit);
		return true;
	}
	else
	{
		cout << "Error! A positive, non-zero value of poptarts must be added!" << endl;
		return false;
	}
}

// Description: Will notify the user that there are no poptarts to dispense.
// Parameters: None
// Returns: Will return false since there are no poptarts to dispense.
bool OutOfPoptart::dispense(void)
{
	cout << "Error! Out of poptarts! Can't dispense poptarts!" << endl;
	return false;
}
// end of Out of poptart state
//////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////
// No Credit state

// Description: Will add the money inserted to the credit state parameter.
// Now that there is credit, change to the Has_Credit state, so
// the user can select a product.
// Parameter: The amount of money inserted.
// Returns: Return true if money is greater than zero.
//			Return false if money is less than one.
bool NoCredit::insertMoney(int money)
{
	// Check if the amount of money is valid.
	if (money > 0)
	{
		// Record how much credit the user inserted.		
		// Change to a HasCredit state now that the user has credit, 
		// so an item can be selected.
		this->CurrentContext->setStateParam(Credit, money);
		cout << "Current credit is " << this->CurrentContext->getStateParam(Credit) << endl;
		this->CurrentContext->setState(Has_Credit);
		return true;
	}
	else
	{
		cout << "Error! Please insert a positive, non-zero value for money!" << endl;
		return false;
	}
}

// Description: Notify the user that a selection can't be made because they have no credit.
// Parameter: User's selected option.
// Returns: Will return false since there is no credit.
bool NoCredit::makeSelection(int option)
{
	cout << "Error! No credit! Can't make a selection!" << endl;
	return false;
}

// Description: Tells the user that their money is being rejected.
// Parameters: None
// Returns: Will return true since the money was rejected.
bool NoCredit::moneyRejected(void)
{
	cout << "Ejecting money!" << endl;
	return true;
}

// Description: Will notify the user that poptarts cannot be added in the current state.
// Parameter: Number of poptarts to add to the dispenser.
// Returns: Will return false since poptarts cannot be added.
bool NoCredit::addPoptart(int number)
{
	cout << "Error! There are already poptarts in the inventory!" << endl;
	return false;
}

// Description: Will tell the user that they have no credit and	no item to dispense.
// Parameters: None
// Returns: Will return false since there is no item to dispense in this state.
bool NoCredit::dispense(void)
{
	cout << "Error! No credit and no item to dispense!" << endl;
	return false;
}
// end of No Credit state
//////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////
// Has Credit state

// Description: Adds the money given to the current credit.
// Parameter: The amount of money inserted.
// Returns: Will return true if money is greater than zero.
//			Will return false if money is less than one.
bool HasCredit::insertMoney(int money)
{
	// Check for a valid amount of money
	if (money > 0)
	{
		// Add the inserted money to the current credit.
		// Remain in the has credit state until a selection is made.
		this->CurrentContext->setStateParam(Credit, this->CurrentContext->getStateParam(Credit) + money);
		cout << "Current credit is now " << this->CurrentContext->getStateParam(Credit) << endl;
		return true;
	}
	else
	{
		cout << "Error! Insert a positive, non-zero value for money!" << endl;
		return false;
	}
}

// Description: Will set the prodcut to the poptart the user selected. Then check for 
// sufficient credit. If the current credit is insufficient, notify the user. 
// Otherwise, subtract the cost of the item from the credit. Then notify the user 
// of the selection made. Now that there is a poptart to dispense, 
// transition to the DispensesPoptart state.
// Parameter: The option number that represents the poptart the user selected.
// Returns: Will return true if a valid poptart has been selected and there is sufficient credit.
//			Returns false if an invalid selection was made or there is not enough credit.
bool HasCredit::makeSelection(int option)
{
	Poptart_Dispenser* dispenser = dynamic_cast<Poptart_Dispenser*>(this->CurrentContext);

	switch (option)
	{
	case 1: //Plain
		dispenser->DispensedItem = new Plain();
		break;
	case 2: //Spicy 
		dispenser->DispensedItem = new Spicy();
		break;
	case 4: //Chocolate
		dispenser->DispensedItem = new Chocolate();
		break;
	case 8: //Coconut
		dispenser->DispensedItem = new Coconut();
		break;
	case 16: //Fruity
		dispenser->DispensedItem = new Fruity();
		break;
	default: //Bad selection
		cout << "Error! Bad selection made!" << endl;
		return false;
		break;
	}

	// Check if the user has enough credit to purchase their selection.
	// If the current credit is insufficient, then notify the user of the deficiency and remove the poptart.
	// Otherwise subtract the cost of the item from the current credit and 
	// Transition to DispensesPoptart state to dispense the product.
	if (this->CurrentContext->getStateParam(Credit) < this->CurrentContext->getStateParam(Cost_Of_Poptart))
	{
		cout << "Error! You do not have enough credit. Please add ";
		cout << this->CurrentContext->getStateParam(Cost_Of_Poptart) - this->CurrentContext->getStateParam(Credit) << endl;
		delete dispenser->DispensedItem;
		dispenser->DispensedItem = nullptr;
		return false;
	}
	else
	{
		cout << dispenser->DispensedItem->description() << " selected!" << endl;
		this->CurrentContext->setStateParam(Credit, this->CurrentContext->getStateParam(Credit) - this->CurrentContext->getStateParam(Cost_Of_Poptart));
		this->CurrentContext->setState(Dispenses_Poptart);
		return true;
	}
}

// Description: Ejects money currently available in credit.
// Parameters: None
// Returns: Will return true since the money was rejected.
bool HasCredit::moneyRejected(void)
{
	// Update the credit to zero since the dispenser is ejecting the credit.
	// Now that there is no credit, we transition back to a NoCredit state.
	this->CurrentContext->setStateParam(Credit, 0);
	this->CurrentContext->setState(No_Credit);
	cout << "Ejecting money!" << endl;
	return true;
}

// Description: Tells the user poptarts cannot currently be added.
// Parameter: Number of poptarts to add.
// Returns: Will return false since poptarts cannot be added.
bool HasCredit::addPoptart(int number)
{
	cout << "Error! You cannot currently add poptarts! User has credit." << endl;
	return false;
}

// Description: Will tell the user that there is nothing to dispsense
// in this state, since the user hasn't selected a product.
// Parameters: None
// Returns: Will return false since there is nothing to dispense.
bool HasCredit::dispense(void)
{
	cout << "Error! Nothing to dispense! User has credit but has not made a selection." << endl;
	return false;
}
// end of Has Credit
//////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////
// Dispenses Poptart state

// Description: Will notify the user that money cannot be inserted during this state.
// Parameter(s): The amount of money to insert.
// Returns: Will return false since money is not accepted while a poptart is being dispensed.
bool DispensesPoptart::insertMoney(int money)
{
	cout << "Error! You cannot insert money! Dispensing item." << endl;
	return false;
}

// Description: Will notify the user that a selection was already made.
// Parameter(s): The poptart option that was selected.
// Returns: Will return false since a selection cannot be made while a poptart is being dispensed.
bool DispensesPoptart::makeSelection(int option)
{
	cout << "Error! Selection was already made. Dispensing item." << endl;
	return false;
}

// Description: Notifies the user that the money is being ejected.
// Parameter(s): none
// Returns: Will return true since the money was rejected.
bool DispensesPoptart::moneyRejected(void)
{
	cout << "Ejecting money!" << endl;
	this->CurrentContext->setStateParam(Credit, 0);
	return true;
}

// Description: Notifies the user that poptarts cannot be added while a poptart is being dispensed.
// Parameter: The number of poptarts to add.
// Returns: Will return false since poptarts cannot be added
//			while a poptart is being dispensed.
bool DispensesPoptart::addPoptart(int number)
{
	cout << "Error! Poptarts cannot be added while dispensing a poptart!" << endl;
	return false;
}

// Description: Will dispense the poptart by telling the dispenser that
// an item was dispensed, but not retrieved, subtracting the poptart
// from the inventory, and notifying the user.
// Then inventory and credit are both checked to determine which state to transition to.
// Parameters: None
// Returns: Will return true since the the poptart is being dispensed. 
bool DispensesPoptart::dispense(void)
{
	Poptart_Dispenser* dispenser = dynamic_cast<Poptart_Dispenser*>(this->CurrentContext);
	
	// Decrement the poptart inventory count.
	// Tell the dispenser that an item has been dispensed, but not retrieved.
	// Notify the user that the product was dispensed.
	this->CurrentContext->setStateParam(No_Of_Poptarts, this->CurrentContext->getStateParam(No_Of_Poptarts) - 1);
	dispenser->itemDispensed = true;
	dispenser->itemRetrieved = false;
	cout << "Poptart has been dispensed! Please retrieve your poptart." << endl;

	// If there are poptarts in the inventory, then check if there is still credit.
	// If there is credit, return to the HasCredit state to allow the user to make another selection.
	// Otherwise, transition to the NoCredit state to wait for a new transaction to begin.
	// If there are no more poptarts in the inventory, then eject any credit remaining
	// and transition to the Out_Of_Poptart state.
	if (this->CurrentContext->getStateParam(No_Of_Poptarts) > 0)
	{
		if (this->CurrentContext->getStateParam(Credit) > 0)
		{
			this->CurrentContext->setState(Has_Credit);
		}
		else
		{
			this->CurrentContext->setState(No_Credit);
		}
	}
	else
	{
		cout << "Error! Out of poptarts!" << endl;
		if (this->CurrentContext->getStateParam(Credit) > 0)
			this->moneyRejected();
		this->CurrentContext->setState(Out_Of_Poptart);
	}

	return true;
}
// end of Dispenses State
//////////////////////////////////////////////////////////////////////////



///////////////////////////////////
// For testing, delete when done
int main()
{
	Poptart_Dispenser pd(0);
	Product* poptart = nullptr;

	///// Test all state methods /////

	// No poptarts
	cout << "No Popcarts!" << endl;
	pd.insertMoney(34);
	pd.makeSelection(8);
	pd.moneyRejected();
	pd.dispense();
	pd.addPoptart(3); // go to No Credit
	cout << endl;

	// No Credit
	cout << "No credit!" << endl;
	pd.addPoptart(3);
	pd.makeSelection(8);
	pd.moneyRejected();
	pd.dispense();
	pd.insertMoney(200); // go to Has Credit
	cout << endl;

	// Has credit
	cout << "Has credit!" << endl;
	pd.addPoptart(3);
	pd.dispense();	
	pd.insertMoney(150);
	pd.moneyRejected(); // go to no credit
	cout << endl;

	// No Credit
	cout << "No Credit!" << endl;
	pd.insertMoney(200); // go to has credit
	cout << endl;

	// Has credit
	cout << "Has Credit!" << endl;
	pd.makeSelection(8); // go to dispense
	cout << endl;

	// dispense
	cout << "Dispense!" << endl;
	pd.addPoptart(3);
	pd.insertMoney(234);
	pd.makeSelection(4);
	pd.dispense();
	cout << endl;

	cout << "Retrieving poptart!" << endl;
	poptart = pd.getProduct();

	if (poptart != nullptr)
		cout << "Producut retrieved: " << poptart->description() << endl;

	cout << endl;
	system("pause");
	return 0;

	//////// Test Menu ///////
	int option = -1;
	int credit = 0;
	int poptartCount = 0;
	char exitMenu = 0;
	
	while (exitMenu != 'y')
	{
		switch (pd.getStateIndex())
		{
		case Out_Of_Poptart:
			cout << "Would you like to exit? (y/n): ";
			cin >> exitMenu;
			cout << endl;
			if (exitMenu != 'y')
			{
				cout << "How many poptarts should be added? ";
				cin >> poptartCount;
				pd.addPoptart(poptartCount);
				cout << endl;
			}
			break;
		case No_Credit:
			cout << "Current credit is 0. Enter some credit: ";
			cin >> credit;
			pd.insertMoney(credit);
			cout << endl;
			break;
		case Has_Credit:			
			cout << "1. Add more credit\n";
			cout << "2. Eject Credit\n";
			cout << "3. Make selection\n";
			cout << "Select an option: ";
			cin >> option;
			cout << endl;

			switch (option)
			{
			case 1: // add credit
				cout << "Enter some credit: ";
				cin >> credit;
				pd.insertMoney(credit);
				break;
			case 2: // eject credit
				pd.moneyRejected();
				break;
			case 3: // make selection
				cout << "Current Credit: " << pd.getStateParam(Credit) << endl;
				cout << "1. Plain (100)\n";
				cout << "2. Spicy (150)\n";
				cout << "3. Chocolate (200)\n";
				cout << "4. Coconut (200)\n";
				cout << "5. Fruity (200)\n";
				cout << "Select an option: ";
				cin >> option;
				switch (option)
				{
				case 1:
					pd.makeSelection(1);
					break;
				case 2:
					pd.makeSelection(2);
					break;
				case 3:
					pd.makeSelection(4);
					break;
				case 4:
					pd.makeSelection(8);
					break;
				case 5:
					pd.makeSelection(16);
					break;
				default:
					pd.makeSelection(0);
					break;
				}
				break;
			default:
				break;
				cout << "Bad input!" << endl;
			}
			cout << endl;
			break;
		case Dispenses_Poptart:
			if (pd.dispense())
			{
				poptart = pd.getProduct();
				if (poptart != nullptr)
				{
					cout << poptart->description() << " retrieved! Eating..." << endl;
					cout << endl;
					delete poptart;
					poptart = nullptr;
				}
			}
			break;
		default:
			break;
		}
	}

	cout << endl;
	system("pause");
	return 0;
}
//////////////////////////////////